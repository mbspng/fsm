"""
MIT License

Copyright (c) 2021 Matthias Bisping

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""

import os
import re
import subprocess
import sys
from functools import partial
from itertools import combinations, product
from operator import truth
from typing import List

import numpy as np
from more_itertools import flatten
from scipy import sparse
from tqdm import tqdm

try:
    from fsm.utils import find_equivalence_classes, invocation_counter
except ModuleNotFoundError:
    from utils import find_equivalence_classes


class Transition:
    def __init__(self, source_state, input_symbol, output_symbol, target_state, weight=1):
        self.__source_state = source_state
        self.__input_symbol = input_symbol
        self.__output_symbol = output_symbol
        self.__target_state = target_state
        self.__weight = weight

    def __eq__(self, other):
        return (
            (self.source_state == other.source_state)
            and (self.input_symbol == other.input_symbol)
            and (self.output_symbol == other.output_symbol)
            and (self.target_state == other.target_state)
        )

    def __hash__(self):
        return hash((self.source_state, self.input_symbol, self.output_symbol, self.target_state))

    @property
    def weight(self):
        return self.__weight

    @weight.setter
    def weight(self, weight):
        self.__weight = weight

    @property
    def source_state(self):
        return self.__source_state

    @source_state.setter
    def source_state(self, state):
        self.__source_state = state

    @property
    def input_symbol(self):
        return self.__input_symbol

    @property
    def output_symbol(self):
        return self.__output_symbol

    @property
    def target_state(self):
        return self.__target_state

    @target_state.setter
    def target_state(self, state):
        self.__target_state = state

    def __repr__(self):
        return f"{self.source_state} -- {self.input_symbol} : {self.output_symbol} -> {self.target_state}"


class TransitionMap(dict):
    pass


class FSMState(int):
    pass


class FiniteStateMachine:
    def __init__(self):
        """Implements a finite state machine for the purposes of visualizing a collection of utterances (intended for
        not more than several hundred utterances) in the context of data set design for chat bot / NLU development.
        The visualized data set is annotated with transition frequencies and transition edges are drawn with a
        proportional width. The purpose is to gain an overview over ones data set to determine balance of utterance
        patterns and to spot possible holes in the set of patterns.

        Examples:
            >>> fsm = FiniteStateMachine()
            >>> fsm.build_from_sequences([['this', 'is', 'a', 'test'], ['this', 'is', 'another', 'test']])
            >>> fsm.minimize()
            >>> fsm.show()
        """
        self.__final_states = set()
        self.__start_states = {FSMState(0)}
        self.__states = self.start_states.copy()
        self.__transitions = (
            {}
        )  # Dict[TransitionMap], index by state to get TransitionMap, index by symbol to get Set of Transition
        self.__current_states = set()
        self.__sink_state = None

    @property
    def sink_state(self):
        return self.__sink_state

    @sink_state.setter
    def sink_state(self, state):
        self.__sink_state = state

    @property
    def current_states(self):
        return self.__current_states

    @current_states.setter
    def current_states(self, states):
        self.__current_states = states

    @property
    def transitions(self):
        return self.__transitions

    @transitions.setter
    def transitions(self, transitions):
        self.__transitions = transitions

    @property
    def start_states(self):
        return self.__start_states

    @property
    def final_states(self):
        return self.__final_states

    @final_states.setter
    def final_states(self, states):
        self.__final_states = states

    @property
    def source_states(self):
        return self.transitions.keys()

    @property
    def states(self):
        return self.__states

    @property
    def non_final_states(self):
        return self.states.difference(self.final_states)

    def num_states(self):
        return len(self.states)

    def __next_available_state_id(self):
        return len(self.states)  # +1 # adjust for sink state, which has ID 0

    def __reset_current_states(self):
        self.__current_states = self.start_states.copy()

    def __next_transitions_from_state_with_symbol(self, state, symbol):
        try:
            return self.transitions[state][symbol]
        except KeyError:
            return None

    def __next_transitions_with(self, symbol):
        get_next_transitions = partial(self.__next_transitions_from_state_with_symbol, symbol=symbol)

        transitions_per_current_state = [*flatten(filter(truth, map(get_next_transitions, self.current_states)))]

        return transitions_per_current_state

    def __source_state_exists(self, source_state):
        return source_state in self.source_states

    def __edge_from_source_state_with_input_symbol_exists(self, source_state, input_symbol):
        return input_symbol in self.transitions[source_state]

    def __add_transition_to_state(self, transition):

        input_symbol_2_transitions = self.transitions[transition.source_state]

        if transition.input_symbol in input_symbol_2_transitions:
            input_symbol_2_transitions[transition.input_symbol].add(transition)
        else:
            input_symbol_2_transitions[transition.input_symbol] = {transition}

    def __add_transition_from_state(self, transition):
        self.transitions[transition.source_state] = TransitionMap()
        self.__add_transition_to_state(transition)

    def __auto_add_transition(self, input_symbol, output_symbol=None):

        target_state = self.__next_available_state_id()
        self.__add_state(target_state)
        for state in self.current_states:
            transition = Transition(state, input_symbol, output_symbol, target_state)
            self.__add_transition(transition)

    def __add_transition(self, transition):

        if self.__source_state_exists(transition.source_state):
            self.__add_transition_to_state(transition)
        else:
            self.__add_transition_from_state(transition)

    def __transit(self, transitions, from_states=None):
        if from_states is None:
            from_states = set(self.current_states)
        assert all(t.source_state in set(from_states) for t in transitions)
        self.current_states = [t.target_state for t in transitions]
        assert len(self.current_states) <= 1
        return self.current_states

    def __make_state_final(self, state):
        assert state in self.states
        self.final_states.add(state)

    def __make_states_final(self, states):
        for state in states:
            self.__make_state_final(state)

    @staticmethod
    def __increment_weights(transitions):
        for transition in transitions:
            transition.weight += 1

    def __add_sequence(self, sequence):

        self.__reset_current_states()

        for symbol in sequence:

            transitions = self.__next_transitions_with(symbol)
            self.__increment_weights(transitions)

            if not transitions:
                self.__auto_add_transition(symbol)
                transitions = self.__next_transitions_with(symbol)

            self.__transit(transitions)

        self.__make_states_final(self.current_states)

    def __any_final(self, states):
        return set(states).intersection(self.final_states) != set()

    def __transition_on_symbol(self, symbol, count_traversals=False):
        transitions = self.__next_transitions_with(symbol)
        self.__transit(transitions)
        if count_traversals:
            self.__increment_weights(transitions)

    def read_sequence(self, sequence, count_traversals=False):

        self.__reset_current_states()

        for symbol in sequence:
            self.__transition_on_symbol(symbol, count_traversals)

        return self.__any_final(self.current_states)

    def is_deterministic(self):
        return all(
            len(transition_set) == 1
            for transition_map in self.transitions.values()
            for transition_set in transition_map.values()
        )

    def build_from_sequences(self, sequences: List):
        """Builds automaton from sequences.

        Args:
            sequences (iterable of iterables of alphabet symbols, e.g. a list of words): sequences to build automaton
            from so that is accepts exactly these sequences.

        Examples:
            >>> fsm = FiniteStateMachine()
            >>> fsm.build_from_sequences([['this', 'is', 'a', 'test'], ['this', 'is', 'another', 'test']])
        """
        self.__reset_current_states()

        for sequence in tqdm(sequences, desc="building automaton from sequences", disable=True):
            self.__add_sequence(sequence)

    def judge(self, sequence, *sequences):
        """Accepts or rejects sequences.

        Args:
            sequence (iterable of alphabet symbols, e.g. a list of words): first sequence to judge.
            sequences (iterable of iterables of alphabet symbols, e.g. lists of words): remaining sequences to judge.

        Returns:
            A sequence of booleans co-indexed with the argument sequences. Each bool codes for whether the sequence was
            accepted or not.

        Examples:
            >>> fsm = FiniteStateMachine()
            >>> fsm.build_from_sequences([['this', 'is', 'a', 'test'], ['this', 'is', 'another', 'test']])
            >>> fsm.judge(['this', 'is', 'a', 'test'])
            True
            >>> fsm.judge(['this', 'is', 'a', 'fish'])
            False
        """

        def inner(sequence, *sequences):

            for sequence in [sequence, *sequences]:
                yield self.read_sequence(sequence)

        if not sequences:
            return next(inner(sequence))

        else:
            return inner(sequence, sequences)

    def to_dot(self):
        """Encodes automaton in dot format.

        Returns:
             string describing the automaton in dot format.
        """
        edges = [
            (e.source_state, e.input_symbol, e.output_symbol, e.target_state, e.weight)
            for transition_map in self.transitions.values()
            for transitions in transition_map.values()
            for e in transitions
        ]
        # TODO: scale with min and max edge weight
        edges = [
            f'{src} -> {trgt} [label="{re.escape(inlbl)}:{weight}", penwidth={np.sqrt(weight + 1) * 2}, color="#000000'
            f'{np.sqrt(weight + 1) * 10}"] '
            for (src, inlbl, outlbl, trgt, weight) in edges
            if self.sink_state not in {src, trgt}
        ]
        edges = "\t" + "\n\t".join(edges)

        finals_line = "node [shape = doublecircle];" + " ".join(map(str, self.final_states)) + ";"

        s = (
            "digraph {\n"
            + "rankdir=LR;"
            + 'edge [lblstyle="above, sloped"];'
            + finals_line
            + "node [shape = circle];"
            + f"{edges}"
            + "\n}"
        )

        return s

    def is_fsm(self):
        # TODO: check if all output symbols are None
        return True

    def __input_alphabet(self):

        return set(
            transition.input_symbol
            for transition_map in self.transitions.values()
            for transition_set in transition_map.values()
            for transition in transition_set
        )

    def __has_outgoing_edges(self, state):
        return state in self.transitions

    def __input_symbols_from_state(self, state):
        return set(self.transitions[state].keys()) if self.__has_outgoing_edges(state) else set()

    def __remove_state_from_states(self, state):
        if state in self.states:
            self.states.remove(state)

    def __remove_state_from_start_states(self, state):
        if state in self.start_states:
            self.start_states.remove(state)

    def __remove_state_from_final_states(self, state):
        if state in self.final_states:
            self.final_states.remove(state)

    def __remove_transitions_from_state(self, state):
        if state in self.transitions:
            self.transitions.pop(state)

    def __remove_transitions_to_state(self, state):
        self.transitions = {
            st: {sym: {t for t in tset if not t.target_state == state} for sym, tset in tmap.items()}
            for st, tmap in self.transitions.items()
        }

    def __incoming_transitions(self, state):
        return {
            t for tmap in self.transitions.values() for tset in tmap.values() for t in tset if t.target_state == state
        }

    def __outgoing_transitions(self, state):
        if self.__source_state_exists(state):
            return {t for tset in self.transitions[state].values() for t in tset}
        else:
            return set()

    def __remove_state(self, state):
        self.__remove_state_from_states(state)
        self.__remove_state_from_start_states(state)
        self.__remove_state_from_final_states(state)
        self.__remove_transitions_from_state(state)
        self.__remove_transitions_to_state(state)

    def __transition_exists(self, transition):
        try:
            return transition in self.transitions[transition.source_state][transition.input_symbol]
        except KeyError:
            return False

    def __add_state(self, state):
        self.states.add(state)

    def __sink_transitions(self, state):
        return self.__input_alphabet().difference(self.__input_symbols_from_state(state))

    # TODO:
    # optimize with https://hal-upec-upem.archives-ouvertes.fr/hal-00620274/document
    # or build minimal from the start https://www.aclweb.org/anthology/J00-1002.pdf
    def minimize(self):
        def merge_states(states):

            representative = min(states)

            for state in states:

                incoming_transitions = self.__incoming_transitions(state)
                outgoing_transitions = self.__outgoing_transitions(state)

                for transition in incoming_transitions:
                    if transition.source_state == representative:
                        raise Exception(
                            f"Transition {transition} will be cyclic once {representative} is made the target state"
                        )
                    transition.target_state = representative
                    if not self.__transition_exists(transition):
                        self.__add_transition(transition)

                for transition in outgoing_transitions:
                    transition.source_state = representative
                    if transition.source_state == transition.target_state:
                        raise Exception(f"Transition {transition} is cyclic")
                    if not self.__transition_exists(transition):
                        self.__add_transition(transition)

                if state != representative:
                    self.__remove_state(state)

            if self.final_states.intersection(states):
                self.__make_state_final(representative)

        def merge_equivalent_states():
            for set_of_equivalent_states in tqdm(
                find_sets_of_equivalent_states(), desc="merging equivalent states", disable=True
            ):
                merge_states(set_of_equivalent_states)

        def common_input_symbols(s1, s2):
            return self.__input_symbols_from_state(s1).intersection(self.__input_symbols_from_state(s2))

        def is_final(state):
            return state in self.final_states

        def make_inequivalence_table():
            n = self.num_states()
            return sparse.triu(sparse.csr_matrix((n, n)), format="lil")

        def iterate_state_pairs():
            return combinations(self.states, 2)

        def get_state_pairs_reachable_per_common_input_symbol_from(s1, s2):
            def get_target_states(symbol):
                def get_target_state(state):
                    transition_set = self.transitions[state][symbol]
                    assert len(transition_set) == 1  # automaton needs to be deterministic for this minimization
                    return max(transition_set).target_state

                return map(get_target_state, (s1, s2))

            return set(map(get_target_states, common_input_symbols(s1, s2)))

        def can_be_proven_inequivalent(s1, s2):
            def final_and_non_final_pair():
                return is_final(s1) ^ is_final(s2)

            def outgoing_input_symbols_differ(s1, s2):
                return self.__input_symbols_from_state(s1) != self.__input_symbols_from_state(s2)

            assert not final_and_non_final_pair(), "Got final and non-final state; something went wrong."

            # if the symbols out of the states differ, they cannot be equivalent
            if outgoing_input_symbols_differ(s1, s2):
                return True

            target_pairs = get_state_pairs_reachable_per_common_input_symbol_from(s1, s2)

            # if there are no common input symbols and none of the states is final, then they cannot be equivalent
            # bug: in some cases states not equiv, but detected as such
            if not target_pairs and not any(map(is_final, [s1, s2])):
                return True

            return any(already_proven_inequivalent(s3, s4) for s3, s4 in target_pairs)

        def already_proven_inequivalent(s1, s2):
            s1, s2 = sorted([s1, s2])  # use only one triangle of the matrix, to that end sort the pair
            return inequivalence_table[s1, s2]

        def mark_as_inequivalent(s1, s2):
            s1, s2 = sorted([s1, s2])  # use only one triangle of the matrix, to that end sort the pair
            inequivalence_table[s1, s2] = 1

        def mark_pairs_of_final_and_non_final_states():

            pairs_of_final_and_non_final_states = list(product(self.non_final_states, self.final_states))

            for s1, s2 in tqdm(
                pairs_of_final_and_non_final_states, desc="identifying inequivalent states (base)", disable=True
            ):
                mark_as_inequivalent(s1, s2)

        def inequivalent_pairs():
            return set(zip(*inequivalence_table.nonzero()))

        def pairs_of_states_not_yet_known_to_be_inequivalent():

            return pairs_of_states.difference(inequivalent_pairs())

        @invocation_counter("iteration")
        def mark_pairs_of_other_states_that_cannot_be_equivalent(iteration):

            nonlocal new_inequivalent_states_discovered

            new_inequivalent_states_discovered = False

            for s1, s2 in tqdm(
                pairs_of_states_not_yet_known_to_be_inequivalent(),
                desc=f"identifying inequivalent states (induction); iteration {iteration}",
                disable=True,
            ):
                if can_be_proven_inequivalent(s1, s2):
                    mark_as_inequivalent(s1, s2)
                    new_inequivalent_states_discovered = True

        def states_form_a_continuous_discrete_range():
            return all(s in self.states for s in range(min(self.states), max(self.states)))

        def find_sets_of_equivalent_states():

            nonlocal inequivalence_table

            # invert the matrix bitwise to make 1s code for equivalent states (as opposed to inequivalent ones as
            # before)
            inequivalence_table = inequivalence_table.todense()
            inequivalence_table = sparse.triu(np.ones_like(inequivalence_table) - inequivalence_table, format="lil")

            # interpret pairs of equivalent states as edges in a graph. Then apply union find
            # to identify equivalence classes. An equivalence class in this case is a set of all
            # equivalent states.
            graph_edges = list(zip(*inequivalence_table.nonzero()))
            sets_of_equivalent_states = [s for s in find_equivalence_classes(graph_edges) if len(s) > 1]

            return sets_of_equivalent_states

        if not self.is_deterministic():
            raise Exception("Cannot minimize non-deterministic automaton.")
        if not self.is_fsm():
            raise Exception("Automaton has output tape labels; minimizing transducers is not implemented.")
        if not states_form_a_continuous_discrete_range():
            raise Exception("States do not form a continuous discrete range.")

        print("Minimizing. This may take a while...", file=sys.stderr)

        # Encodes states to not be equivalent by the value 1. 0 means unknown equivalence until algorithm completes;
        # then it means equivalence.
        inequivalence_table = make_inequivalence_table()

        pairs_of_states = set(iterate_state_pairs())

        # These states cannot be equivalent since they cannot accept the same language by virtue of one being final
        # and the other one not.
        mark_pairs_of_final_and_non_final_states()

        new_inequivalent_states_discovered = inequivalent_pairs() != set()
        while new_inequivalent_states_discovered:
            mark_pairs_of_other_states_that_cannot_be_equivalent()

        merge_equivalent_states()

    def show(self, tmp_path="/tmp", program="firefox", img_format="svg"):
        """Encodes the automaton under `tmp_path` as graphviz code and renders it with dot (per default to SVG). Opens
        it with a SVG capable program (defaulting to Firefox).

        Args:
            tmp_path (str): path where to write code for render to
            program (str): program to open render in
            img_format (str): image format (png, svg, ...)
        """
        dot_file = os.path.join(tmp_path, "fsm.dot")
        img_file = os.path.join(tmp_path, f"fsm.{img_format}")

        with open(dot_file, "w") as f:
            f.write(self.to_dot())

        process = subprocess.Popen(["dot", "-T", img_format, dot_file, "-o", img_file])
        process.wait()

        subprocess.Popen([program, img_file])


if __name__ == "__main__":
    import doctest

    doctest.testmod()

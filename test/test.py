from fsm import FiniteStateMachine
from spacy.lang.en import English
import pandas as pd


def get_texts_from_csv():
    texts = pd.read_csv('data/texts.csv').sent
    texts = texts.sort_values()
    n = 10000
    return texts.iloc[n:n + 100].values


def get_texts_from_txt():
    with open('data/texts.txt') as f:
        return [l.strip() for l in f]


def test():
    nlp = English()
    tokenizer = nlp.tokenizer

    def tokenize(text):
        return [e.text for e in tokenizer(text)]

    texts = get_texts_from_csv()

    tokens_per_text = [*map(tokenize, texts)]

    fsm = FiniteStateMachine()
    fsm.build_from_sequences(tokens_per_text)
    fsm.minimize()
    fsm.show()


if __name__ == '__main__':
    test()

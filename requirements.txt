numpy~=1.20.2
scipy~=1.6.3
tqdm~=4.60.0
spacy~=3.0.6
pandas~=1.2.4
more-itertools~=9.0.0

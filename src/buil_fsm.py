from fsm import FiniteStateMachine
from spacy.lang.en import English
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("file", help="one phrase per line")
args = parser.parse_args()

with open(args.file) as f:
    lines = f.readlines()

nlp = English()
tokenizer = nlp.tokenizer


def tokenize(text):
    return [e.text for e in tokenizer(text)]


tokens_per_text = [*map(tokenize, lines)]

fsm = FiniteStateMachine()
fsm.build_from_sequences(tokens_per_text)
fsm.minimize()
fsm.show(img_format='png')
print("Wrote fsm to /tmp/fsm.dot and /tmp/fsm.png")

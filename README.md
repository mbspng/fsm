# Overview

This module implements a finite state machine for the purposes of visualizing a collection of utterances (intended for
not more than several hundred utterances) in the context of data set design for chat bot / NLU development.
The visualized data set is annotated with transition frequencies and transition edges are drawn with a
proportional width. The purpose is to gain an overview over ones data set to determine balance of utterance
patterns and to spot possible holes in the set of patterns.

# Installation

## Install module

```bash
git clone git@gitlab.com:mbspng/fsm.git
cd fsm
pip install -e .
pip install -r requirements.txt
```

## Install Graphviz

Ubuntu or something? Do this:

```bash
sudo apt-get install graphviz
```
Windows or something? Go here I guess: https://graphviz.org/


# Usage Example


```python
from fsm import FiniteStateMachine
from spacy.lang.en import English

nlp = English()
tokenizer = nlp.tokenizer

def tokenize(text):
    return [e.text for e in tokenizer(text)]

texts = [
    'can you show how this works',
    'please, show me how this works',
    'can you show how this works, please?',
    'can you give me an example, please?',
    'an example, can I have one?',
    'an example would be great'
] 

tokens_per_text = [*map(tokenize, texts)]

fsm = FiniteStateMachine()
fsm.build_from_sequences(tokens_per_text)
fsm.minimize()
fsm.show(img_format='png')
```

![Example Automaton](/img/fsm.png "Example")



